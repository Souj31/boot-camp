package ThirdAssesment;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class ThirdAssesment {
	
	public Statement getStatement()
	{
		Connection con = null;
        Statement stmt = null;
		
		
		try
		{
			Class.forName("com.mysql.jdbc.Driver");
			 con=DriverManager.getConnection("jdbc:mysql://localhost:3306/Assesment3","root","password");
			 
			 stmt = con.createStatement();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return stmt;
	}
		
		public void full_name()
		{
			String sql;
			try 
			{
				 sql = "select concat(UPPER (First_Name),  ' ', UPPER (last_Name)) as Full_Name from WorkerTable1";
				 ResultSet rs = getStatement().executeQuery(sql);
				 
				 System.out.println("*****FullName*****");
				 while(rs.next())
				 {
					 System.out.println(rs.getString(1));
					 System.out.println(" ");
				 }
			}
			catch(Exception e)
			{
				e.printStackTrace();
			
			}
			
		}
		
		public void distinct() {
		
		String sql;
		try 
		{
			 sql = "select  distinct Department from WorkerTable1";
			 ResultSet rs1=getStatement().executeQuery(sql);
			 System.out.println("*****Distinct Department******");
			 
			 while(rs1.next())
			 {
				 System.out.println(rs1.getString(1));
				 System.out.println(" ");
			 }
		}
		catch(Exception e)
		{
			e.printStackTrace();
		
		}
			
	}
		
		public void position()
		{
			String sql;
			try 
			{
				 sql = "select  instr(binary First_Name,'a') from WorkerTable1 where First_Name = 'Amitabh'";
				 ResultSet rs2=getStatement().executeQuery(sql);
				 System.out.println("*****Position of a******");
				 while(rs2.next())
				 {
					 System.out.println(rs2.getString(1));
				 }
			}
			catch(Exception e)
			{
				e.printStackTrace();
			
			}
		}
			
		
	
	public static void main(String[] args) throws Exception {
		
		
		ThirdAssesment obj = new ThirdAssesment();
		obj.full_name();
		obj.distinct();
		obj.position();
	}
		
		

}
