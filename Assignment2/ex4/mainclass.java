package main;

import java.util.Scanner;

import Test.Registration;

public class mainclass {
	
	private static String FIRSTNAME;
	private static String LASTNAME;
	private static String MailId;
	private static String Address;
	private static long PNO;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Registration rg = new Registration('F') ;

		System.out.println("Registration:");
		Scanner sc = new Scanner(System.in);
        
		
		System.out.println("Please enter First Name");
		FIRSTNAME = sc.nextLine();
		rg.firstName(FIRSTNAME);
		
		System.out.println("Please enter Last Name");
		LASTNAME = sc.nextLine();
		rg.lastName(LASTNAME);
		
		System.out.println("Please enter MailID");
		MailId = sc.nextLine();
		rg.mid(MailId);
		
		System.out.println("Please enter Address");
		Address = sc.nextLine();
		rg.addr(Address);
		
		System.out.println("Please enter Phone Number");
		PNO = sc.nextLong();
		rg.phoneNo(PNO);
	}

}
