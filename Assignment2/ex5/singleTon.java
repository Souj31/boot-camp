package Assignment2;

 class singletn {
		static singletn obj = null;
		
		private singletn() {
			System.out.println("Inside default");
			//private constructor can be called within class only
		}
		
		public static singletn getObj() {
			if(obj == null) {
				obj = new singletn();// bcz of this default constructor is being called
			}
			return obj;
		}
	}
	public class singleTon{
		public static void main(String[] args) {
			// TODO Auto-generated constructor stub
			singletn obj1 = singletn.getObj();
			
			singletn obj2 = singletn.getObj();
			
			System.out.println(obj1.hashCode());
			System.out.println(obj2.hashCode());
		}
	}


